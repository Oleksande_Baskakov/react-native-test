import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { AuthRouter } from './auth/auth.router';

const RootStack = createStackNavigator();
export const Root: React.FC = (): JSX.Element => {
  return (
    <NavigationContainer>
      <RootStack.Navigator initialRouteName="auth" headerMode="none">
        <RootStack.Screen name='auth' component={AuthRouter}/>
      </RootStack.Navigator>
    </NavigationContainer>
  );
}
