import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LogIn from '../../screens/LogIn';
import Main from '../../screens/MainMenu';
// import List from '../../screens/List';

const AuthStack = createStackNavigator();

export const AuthRouter: React.FC = (): JSX.Element => {
  return (
    <AuthStack.Navigator headerMode='none'>
        <AuthStack.Screen name='Log' component={LogIn}/>
        <AuthStack.Screen name='Main' component={Main}/>
        {/* <AuthStack.Screen name='List' component={List}/> */}
    </AuthStack.Navigator>
  );
}
