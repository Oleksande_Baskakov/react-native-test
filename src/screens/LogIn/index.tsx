import { useNavigation } from '@react-navigation/native';
import React, { useCallback } from 'react';
import { FC, useEffect, useState } from 'react';
import { SafeAreaView, Text, TextInput, View, StyleSheet, Button, Alert } from 'react-native';
import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
  } from 'react-native-confirmation-code-field';
import FingerprintScanner from 'react-native-fingerprint-scanner';
import ReactNativeBiometrics from 'react-native-biometrics'
import SInfo from 'react-native-sensitive-info';
// import styles from './styles';


const CELL_COUNT = 4;

const Home: FC = (): JSX.Element => {
  const [value, setValue] = useState('');
  const [touchId, setTouchId] = useState(false);
  const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  const submitHandler = () => {
      if (value !== '1234') {
        return;
      }
      navigate('Main');
  }

  const touchIDHandler = () => {
      FingerprintScanner
        .authenticate({ description: 'Scan your fingerprint on the device scanner to continue' })
        .then(() => {
          Alert.alert('Authenticated successfully');
          navigate('Main');
        })
        .catch((error) => {
          Alert.alert(error.message);
        });
  }
 
  const { navigate } = useNavigation();
  return (
    <SafeAreaView style={styles.root}>
       <View>
            <Text style={styles.title}>Hallo. Please write your password</Text>
            <Text>HINT(ps: 1234)</Text>
            <CodeField
        ref={ref}
        {...props}
        value={value}
        onChangeText={setValue}
        cellCount={CELL_COUNT}
        rootStyle={styles.codeFieldRoot}
        keyboardType="number-pad"
        textContentType="oneTimeCode"
        renderCell={({index, symbol, isFocused}) => (
          <Text
            key={index}
            style={[styles.cell, isFocused && styles.focusCell]}
            onLayout={getCellOnLayoutHandler(index)}>
            {symbol || (isFocused ? <Cursor /> : null)}
          </Text>
        )}
      />       
      <Button title='Next' onPress={submitHandler}></Button>
      <Button title='TouchID' onPress={touchIDHandler}></Button>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
    root: {flex: 1, padding: 20},
    title: {textAlign: 'center', fontSize: 30},
    codeFieldRoot: {marginTop: 20},
    cell: {
      width: 40,
      height: 40,
      lineHeight: 38,
      fontSize: 24,
      borderWidth: 2,
      borderColor: '#00000030',
      textAlign: 'center',
    },
    focusCell: {
      borderColor: '#000',
    },
  });

export default Home;
