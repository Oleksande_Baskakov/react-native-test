import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { FC, useEffect, useState } from 'react';
import { FlatList, Linking, SafeAreaView, Text, TextInput, TouchableOpacity,Button, View, StyleSheet } from 'react-native';
import { Header, Avatar } from 'react-native-elements';
import { RNCamera, FaceDetector } from 'react-native-camera';
import { Icon } from 'react-native-elements/dist/icons/Icon';
import styles from './styles';

const Home: FC = (): JSX.Element => {
  const { navigate } = useNavigation();
  const onChangeText = (e: any) => {
    console.log(e);
  }
  const changeAvatar = () => {
    
  }
  return (
    <SafeAreaView style={styles.safeArea}>
        <Header
            onTouchEnd={changeAvatar}
            leftComponent={{icon: 'menu'}}
            centerComponent={{ text: 'MAIN MENU', style: { color: '#fff' } }}
            rightComponent={<Avatar rounded source={{
            uri: 'https://images.unsplash.com/photo-1522404056954-ceabdcdbcbc7?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cGVhcGxlfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80',
  }}/>}
/>
<RNCamera></RNCamera>
    </SafeAreaView>
  );
}
const styles2 = StyleSheet.create({
  input: {
    minWidth: 100,
    height: 40,
    margin: 12,
    borderWidth: 1,
  },
});

export default Home;
