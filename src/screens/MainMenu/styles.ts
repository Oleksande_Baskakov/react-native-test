import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: '#c8d6e5',
    flex: 1
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 15
  },
  headText: {
    color: '#222f3e',
    fontSize: 32,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  listItem: {
    backgroundColor: '#ffffff',
    marginBottom: 20,
    width: '100%'
  },
  item: {
    borderBottomWidth: 1,
    borderColor: '#c8d6e5',
    color: '#000000',
    fontSize: 16,
    justifyContent: 'space-between',
    padding: 5,
    paddingHorizontal: 20,
    width: '100%'
  },
  number: {
    color: '#2e86de',
    textDecorationLine: 'underline',
  }
});

export default styles;
